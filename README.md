# Recursos para el desarrollo Seguro

## Presentación

Dentro del repositorio podremos encontrar distintos recursos generados internamente o recursos externos para las metodologías y/o snipets para el desarrollo seguro así como las vulnerabilidades más comunes que podemos encontrar en Python.

## Proyecto colaborativo.

El proyecto es colaborativo, por lo que si queréis aportar con correcciones de todo tipo (ortográficas, de código, de herramientas) o cualquier mejora (nuevas herramientas, snipets para mitigaciones, snipets para vulnerablidades, enlaces a recursos,...) estaremos encantados de recibir vuestras peticiones en forma de PR o directamente en el canal de slack #devops-request. 

## Lista de recursos

1. Guía de vulnerabilidades según top ten OWASP 2017 con ejemplos en el lenguaje Python/Django.
2. Aplicaciones vulnerables desarrolladas en python para probar y practicar las vulnerabilidades más comunes listadas en OWASP top ten 2017.
3. Enlaces de interés a recursos externos para el desarrollo seguro.

### 1. Guía de vulnerabilidades según top ten OWASP 2017 con ejemplos en el lenguaje Python/Django.

El fichero lo podréis encontrar en este mismo repositorio: [guia_desarrollo_seguro_owasp_2017-python_django.md](guia_desarrollo_seguro_owasp_2017-python_django.md).

En la guía de vulnerabilidades podréis encontrar un listado de las vulnerabilidades OWASP top ten 2017 con código vulnerable en Python/Django y cómo mitigarlo con código o librerías.
Este fichero está en construcción.

### 2. Aplicaciones vulnerables desarrolladas en python

Para realizar pruebas y entender las vulnerabilidades que podemos encontrar en proyectos Python/Django tenemos los siguientes códigos vulnerables. IMPORTANTE: Estás aplicaciones son muy vulnerables por lo que no es buena idea tenerlas en servidores expuestos a internet o en entornos productivos, es necesario desplegarlas en local y parar el servicio/contenedor/VM cuando ya no sean de utilidad para las pruebas.

### 3. Enlaces de interés a recursos externos

A continuación mostramos la lista de los enlaces de interés.

#### Buenas prácticas de desarrollo

1. OWASP secure coding: [https://owasp.org/www-project-secure-coding-practices-quick-reference-guide/migrated_content](https://owasp.org/www-project-secure-coding-practices-quick-reference-guide/migrated_content)

2. Todos los recursos PDF de OWASP: [https://owasp.org/www-pdf-archive/](https://owasp.org/www-pdf-archive/)

#### S-SDLC

1. OWASP-Latam-tour: Diapositivas de charla sobre S-SDLC: [https://owasp.org/www-pdf-archive/OWASP-LATAMTour-Patagonia-2016-rvfigueroa.pdf](https://owasp.org/www-pdf-archive/OWASP-LATAMTour-Patagonia-2016-rvfigueroa.pdf)




